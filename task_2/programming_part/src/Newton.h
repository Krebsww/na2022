#include<iostream>

class Newton
{
private:
    double *a,*x;
    int n_;
public:
    Newton(){};
    void inter(double (*f) (double),double *x_, int n,double (*f_) (double)=nullptr);
    double pre(double p);
    double dri(double p);
    void print()
    {
        for(int i=0;i<n_;i++) std::cout<<a[i]<<",";
        std::cout<<std::endl;
    }
    ~Newton()
    {
    delete []a;
    delete []x;
    }
};


void Newton::inter(double (*f) (double),double *x_, int n,double (*f_) (double))
{
    n_=n;
    double b[n][n];
    this->a=new double[n];
    this->x=new double[n];
    for(int i=0;i<n;i++)
    {
        x[i]=x_[i];
        for(int j=i;j<n;j++)
        {
            if(i==0) b[j][i]=f(x_[j]);
            else{
                if(x_[j]!=x_[j-i]) b[j][i]=(b[j][i-1]-b[j-1][i-1])/(x_[j]-x_[j-i]);
                else{
                    if(f_!=nullptr) b[j][i]=f_(x_[j]);
                    else{
                        std::cout<<"Derivative information is needed!";
                        std::exit(1);
                    }
                }
                };
            if(i==j) a[i]=b[i][i];
        }
    }
        
}

double Newton::pre(double p)
{
    double t=1,s=0;
    for(int i=0;i<n_;i++)
    {
        s+=t*a[i];
        t*=p-x[i];
    }
    return s;
}

double Newton::dri(double p)
{
    double d=0.0,t=1.0;
    for(int i=1;i<n_;i++)
    {
        t*=p-x[i-1];
        for(int j=0;j<i;j++)
        {
            d+=a[i]*t/(p-x[j]);
        }
    }
    return d;
}