#include<iostream>
#include "Bspline.h"
#include<vector>
#include<cmath>
#include<fstream>
using namespace std;
/* double tx(double th)
{
    return sqrt(3)*cos(th); 
}
double ty(double th)
{
    return (sqrt(abs(sqrt(3)*cos(th)))+sqrt(3)*sin(th))*double(2)/double(3);
} */
double uf(double x)
{
    return (sqrt(fabs(x))+sqrt(3.0-x*x))*2.0/3.0;
}
double lf(double x)
{
    return (sqrt(fabs(x))-sqrt(3.0-x*x))*2.0/3.0;
}

int main()
{
    ofstream fout;
    fout.open("E.txt",ios::out);
    condition_type ct=natural;
    vector<double> x;vector<double> u;vector<double> l;
    int n[]={10,40,160};
    /* for(int i=0;i<3;i++)
    { */
        int i=2;
        int n1 = floor((n[i]- 4.0)/4.0);
        double g=sqrt(3)/n1;
        for(int j=0;j<=2*n1;j++)
        {
            double t=-sqrt(3)+j*g;
            x.push_back(t);
            l.push_back(lf(t));
            u.push_back(uf(t));
        }
        bs_s32 ls(x,l,ct); bs_s32 us(x,u,ct);
        int N=100;
        double s=2.0*sqrt(3)/N;
        fout<<"插值得到的心形曲线的拟合点如下："<<endl;
        for(int i=0;i<=N;i++)
        {
            double t=-sqrt(3)+i*s;
            fout<<t<<" "<<ls.value(t)<<endl;
        }
        for(int i=0;i<=N;i++)
        {
            double t=-sqrt(3)+i*s;
            fout<<t<<" "<<us.value(t)<<endl;
        }
        fout.close();
        return 0;
    /* } */
    
}