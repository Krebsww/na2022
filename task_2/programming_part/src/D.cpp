#include<iostream>
#include<fstream>
#include<cmath>
#include"Newton.h"
using namespace std;
double f(double x);
double f_(double x);
int main()
{
    Newton N;
    double x[10]={0,0,3,3,5,5,8,8,13,13};
    N.inter(f,x,10,f_);
    cout<<"distance:"<<N.pre(10)<<endl<<"speed:"<<N.dri(10);
    ofstream fout;
    fout.open("Da_out.txt",ios::out);
    for(double i=0;i<=13;i+=0.1) fout<<N.pre(i)<<",";
    ofstream fo;
    fo.open("Db_out.txt",ios::out);
    for(double i=0;i<=13;i+=0.1) 
    {
        double t=N.dri(i);
        if(isnan(t)) fo<<f_(i)<<","; 
        else fo<<t<<",";
    }
    fo.close();
    fout.close();
    return 0;
}

double f(double x)
{
    int y;
    switch (int(x))
    {
    case 0:
        y=0;
        break;
    case 3:
        y=225;
        break;
    case 5:
        y=383;
        break;
    case 8:
        y=623;
        break;
    case 13:
        y=993;
        break;
    default:
        break;
    }
    return y;
}
double f_(double x)
{
    int y;
    switch (int(x))
    {
    case 0:
        y=75;
        break;
    case 3:
        y=77;
        break;
    case 5:
        y=80;
        break;
    case 8:
        y=74;
        break;
    case 13:
        y=72;
        break;
    default:
        break;
    }
    return y;
}