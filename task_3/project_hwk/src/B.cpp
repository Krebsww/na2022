#include<iostream>
#include"Bspline.h"
#include<vector>
#include<cmath>
#include<fstream>
using namespace std;
double f(double x)
{
    return 1/(1+25*x*x);
}
double df(double x)
{
    return -50*x/pow(1+25*x*x,2);
}
double ddf(double x)
{
    return 50*(100*x*x*(1+25*x*x)-pow(1+25*x*x,2))/pow(1+25*x*x,4);
}


int main()
{
   ofstream fout;
    fout.open("B./.txt",ios::out);
    int N[]={6,11,21,41,81};
    vector<double> bc={df(-1),df(1)};
    condition_type ct=complete;  
    int s=100;
    fout<<"在如下的点："<<endl;
    double sg=double(2)/s;
    for(int j=0;j<=s;j++)
    {
        double t=-1+j*sg;
        fout<<t<<" ";
    }
    fout<<endl;
    fout<<"不同插值节点数，对应插值函数的值为："<<endl;
    for( int i=1;i<=5;i++)
    {
        vector<double> x;vector<double> y;
        double g=double(2)/N[i-1];
        for(int j=0;j<=N[i-1];j++)
        {
            double t=-1+j*g;
            x.push_back(t);y.push_back(f(t));
        }
        bs_s32 bs(x,y,ct,bc);
        for(int j=0;j<=s;j++)
        {
            double t=-1+j*sg;
            fout<<bs.value(t)<<" ";
        }
        fout<<endl;
    }
    fout.close();
    return 0;
}