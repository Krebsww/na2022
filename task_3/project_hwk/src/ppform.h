#include<iostream>
#include<cmath>
#include<vector>
#include"piecewise_poly.h"
#include"lapacke.h"

enum condition_type{complete,SSD,natural};

class spline
{
protected:
    std::vector<double> kx;
    std::vector<double> ky;
public:
    spline(const std::vector<double>& k1,const std::vector<double>& k2):kx(k1),ky(k2)
    {
        if(k1.size()!=k2.size())
        {
            std::cout<<"The length of two given coordinate arrays doesn't match! ";
            std::exit(1);
        }
    };
    ~spline(){};
    virtual double value(double x)=0;
};

class ppform : public spline
{
protected:
    piecewise_poly poly;
public:
    ppform(const std::vector<double>& k1,const std::vector<double>& k2):spline(k1,k2){};
    ~ppform(){};
    inline double value(double x);
    inline void print();
};

double ppform::value(double x)
{
    return this->poly.value(x);
}
void ppform::print()
{
    poly.print();
}

class pp_linear:public ppform
{
private:
    /* data */
public:
    pp_linear(const std::vector<double>& k1,const std::vector<double>& k2);
    ~pp_linear(){};
};

pp_linear::pp_linear(const std::vector<double>& k1,const std::vector<double>& k2):ppform(k1,k2)
{
    std::vector<polynomial> p;
    p.push_back(polynomial(std::vector<double>{0}));
    for(int i=0;i<k1.size()-1;i++)
    {
        std::vector<double> t;
        double k;
        k=(k2[i+1]-k2[i])/(k1[i+1]-k1[i]);
        t.push_back(k2[i]-k*(k1[i]));
        t.push_back(k);
        p.push_back(polynomial(t));
    }
    p.push_back(polynomial(std::vector<double>{0}));
    this->poly=piecewise_poly(k1,p);
}

class pp_s32:public ppform
{
private:
    std::vector<double> c;
    condition_type ct;
    void solve_linear();
public:
    pp_s32(const std::vector<double>& k1,const std::vector<double>& k2,condition_type ct_,std::vector<double>& c_):ppform(k1,k2),c(c_),ct(ct_)
    {
        if(c_.size()!=2) 
        {
            std::cout<<"The amount of given boundary conditions is not appropriate!";
            std::exit(1);
        }
        this->solve_linear();
    }
    pp_s32(const std::vector<double>& k1,const std::vector<double>& k2,condition_type ct_):ppform(k1,k2),ct(ct_)
    {
        if(ct!=natural)
        {
            std::cout<<"There is lack of  boundry conditions!";
            std::exit(1);
        }
        this->solve_linear();
    }
    ~pp_s32(){};
};

void pp_s32::solve_linear()
{
    bool N=0;
    if(ct==complete)
    {
        int n=kx.size()-2,col=1,info=0;
        double b[n],dd[n+1];
        for(int i=0;i<=kx.size()-2;i++)
        {
            dd[i]=(ky[i+1]-ky[i])/(kx[i+1]-kx[i]);
        }
        if(n==1)
        {
            double u,l;
            u=(kx[1]-kx[0])/(kx[2]-kx[0]); l=(kx[2]-kx[1])/(kx[2]-kx[0]);
            b[0]=double(3)*(u*dd[1]+l*dd[0])-l*c[0]-u*c[1];
            b[0]/=double(2);
        }
        else{
                double u[n-1],l[n-1],d[n];
                for(int i=0;i<n-1;i++)
                {
                    u[i]=(kx[i+1]-kx[i])/(kx[i+2]-kx[i]);
                    l[i]=(kx[i+3]-kx[i+2])/(kx[i+3]-kx[i+1]);
                    d[i]=2;
                    if(i==0) 
                    {
                        double tl=(kx[i+2]-kx[i+1])/(kx[i+2]-kx[i]);
                        b[i]=double(3)*(u[i]*dd[i+1]+tl*dd[i])-tl*c[0];
                        continue;
                    }
                    b[i]=double(3)*(u[i]*dd[i+1]+l[i-1]*dd[i]);
                    if(i==n-2)
                    {
                        double tu=(kx[i+2]-kx[i+1])/(kx[i+3]-kx[i+1]);
                        b[i+1]=double(3)*(tu*dd[n]+l[i]*dd[n-1])-tu*c[1];
                    }
                }
                d[n-1]=2;
                dgtsv_(&n,&col,l,d,u,b,&n,&info);
            }
        
        if(info!=0)
        {
            std::cout<<"Something is wrong with the calculation driven by lapack! Since the INFO is"<<info;
            std::exit(1);
        }

        double m[kx.size()];
        m[0]=c[0];m[kx.size()-1]=c[1];
        for(int i=1;i<kx.size()-1;i++) m[i]=b[i-1];
        std::vector<polynomial> p;
        for(int i=0;i<kx.size()-1;i++)
        {
           polynomial tp(std::vector<double>{1});polynomial s(std::vector<double>{0});
           double a[4];
           a[0]=ky[i];a[1]=m[i];a[2]=(double(3)*dd[i]-double(2)*m[i]-m[i+1])/(kx[i+1]-kx[i]);a[3]=(m[i]+m[i+1]-double(2)*dd[i])/pow((kx[i+1]-kx[i]),2);
            for(int j=0;j<=3;j++)
            {
                s=s+tp*a[j];
                tp=tp*polynomial(std::vector<double>{-kx[i],1});
            }
            p.push_back(s);
            if(i==0||i==kx.size()-2)  p.push_back(s);
        }
        poly=piecewise_poly(kx,p);
    }  

    if(ct==natural)
    {
        N=1;
        c={0,0};
    }
    
    if(ct==SSD||N)
    {
        int n=kx.size()-2,col=1,info=0;
        double b[n],dd[n+1],td[n];
        for(int i=0;i<=kx.size()-2;i++)
        {
            dd[i]=(ky[i+1]-ky[i])/(kx[i+1]-kx[i]);
        }
        for(int i=0;i<=kx.size()-3;i++)
        {
            td[i]=(dd[i+1]-dd[i])/(kx[i+2]-kx[i]);
        }

        if(n==1)
        {
            double u,l;
            u=(kx[1]-kx[0])/(kx[2]-kx[0]); l=(kx[2]-kx[1])/(kx[2]-kx[0]);
            b[0]=double(6)*td[0]-l*c[1]-u*c[0];
            b[0]/=double(2);
        }
        else{
                double u[n-1],l[n-1],d[n];
                for(int i=0;i<n-1;i++)
                {
                    u[i]=(kx[i+2]-kx[i+1])/(kx[i+3]-kx[i+1]);
                    l[i]=(kx[i+2]-kx[i+1])/(kx[i+2]-kx[i]);
                    d[i]=2;
                    if(i==0) 
                    {
                        double tu=(kx[i+1]-kx[i])/(kx[i+2]-kx[i]);
                        b[i]=double(6)*td[0]-tu*c[0];
                        continue;
                    }   

                    b[i]=double(6)*td[i];
                    
                    if(i==n-2)
                    {
                        double tl=(kx[i+3]-kx[i+2])/(kx[i+3]-kx[i+1]);
                        b[i+1]=double(6)*td[n-1]-tl*c[1];
                    }
                }
                d[n-1]=2;
                dgtsv_(&n,&col,u,d,l,b,&n,&info);
            }
        
        if(info!=0)
        {
            std::cout<<"Something is wrong with the calculation driven by lapack! Since the INFO is"<<info;
            std::exit(1);
        }

        double M[kx.size()];
        M[0]=c[0];M[kx.size()-1]=c[1];
        for(int i=1;i<kx.size()-1;i++) M[i]=b[i-1];
        std::vector<polynomial> p;
        for(int i=0;i<kx.size()-1;i++)
        {
           polynomial tp(std::vector<double>{1});polynomial s(std::vector<double>{0});
           double a[4];
           a[0]=ky[i];a[1]=dd[i]-(M[i+1]+M[i]*double(2))*(kx[i+1]-kx[i])/double(6);a[2]=M[i]/double(2);a[3]=(M[i+1]-M[i])/(kx[i+1]-kx[i])/double(6);
            for(int j=0;j<=3;j++)
            {
                s=s+tp*a[j];
                tp=tp*polynomial(std::vector<double>{-kx[i],1});
            }
            p.push_back(s);
            if(i==0||i==kx.size()-2)  p.push_back(s);
        }
        poly=piecewise_poly(kx,p);
    }
}