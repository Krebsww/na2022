#include<iostream>
#include<fstream>
#include"Newton.h"
using namespace std;
double f1(double x);
double f2(double x);
int main()
{
    ofstream fout;
    fout.open("Eb_out4.txt",ios::out);
    Newton N1,N2;
    double x[4]={13,17,20,28};
    N1.inter(f1,x,4);
    N2.inter(f2,x,4);

    for(int i=13;i<=43;i++)
    {
        fout<<N1.pre(i)<<",";
    }
    fout<<endl;
    for(int i=13;i<=43;i++)
    {
        fout<<N2.pre(i)<<",";
    }
    fout.close();
    return 0;
}

double f1(double x)
{
    double y;
    switch (int(x))
    {
    case 0:
        y=6.67;
        break;
    case 6:
        y=17.3;
        break;
    case 10:
        y=42.7;
        break;
    case 13:
        y=37.3;
        break;
    case 17:
        y=30.1;
        break;
    case 20:
        y=29.3;
        break;
    case 28:
        y=28.7;
        break;
    default:
        break;
    }
    return y;
}
double f2(double x)
{
    double y;
    switch (int(x))
    {
    case 0:
        y=6.67;
        break;
    case 6:
        y=16.1;
        break;
    case 10:
        y=18.9;
        break;
    case 13:
        y=15.0;
        break;
    case 17:
        y=10.6;
        break;
    case 20:
        y=9.44;
        break;
    case 28:
        y=8.89;
        break;
    default:
        break;
    }
    return y;
}