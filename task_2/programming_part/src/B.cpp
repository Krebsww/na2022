#include<iostream>
#include<fstream>
#include"Newton.h"
using namespace std;
double f(double x);
int main()
{
    ofstream fout;
    fout.open("Bout.txt",ios::out);
    for(int j=2;j<=8;j+=2)
    {
        const int n=j;
        Newton N;
        double x[n+1];
        for(int i=0;i<=n;i++) x[i]=-5+10*i/n;
        N.inter(f,x,n+1);
        for(int i=0;i<=100;i++)
        {
            double t_x=-5.0+10.0*i/100.0;
            fout<<N.pre(t_x)<<",";
        }
        fout<<endl;
    }
    fout.close();
    return 0;
}

double f(double x)
{
    return 1/(1+x*x);
}
