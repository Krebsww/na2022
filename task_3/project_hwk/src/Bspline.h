#include<iostream>
#include<cmath>
#include<vector>
#include"piecewise_poly.h"
#include"lapacke.h"

enum condition_type{complete,SSD,natural};

class spline
{
protected:
    std::vector<double> kx;
    std::vector<double> ky;
public:
     spline(const std::vector<double>& k1,const std::vector<double>& k2):kx(k1),ky(k2)
    {
        if(k1.size()!=k2.size())
        {
            std::cout<<"The length of two given coordinate arrays doesn't match! ";
            std::exit(1);
        }
    };
    ~spline(){};
    virtual double value(double x)=0;
};

class Bspline:public spline
{
protected:
    piecewise_poly sp;
    std::vector<piecewise_poly> basis;
    std::vector<double> co;
    virtual void get_basis()=0;
    virtual void solve_linear()=0;
public:
    Bspline(const std::vector<double>& k1,const std::vector<double>& k2):spline(k1,k2){};
    ~Bspline(){};
    double value(double x)
    {
        return this->sp.value(x);
    }
    void print()
    {
        this->sp.print();
    }
};

class bs_linear:public Bspline
{
private:
    void get_basis();
    void solve_linear();
public:
    bs_linear(const std::vector<double>& k1,const std::vector<double>& k2):Bspline(k1,k2)
    {
        this->get_basis();
        this->solve_linear();
    }
    ~bs_linear(){};
};

void bs_linear::get_basis()
{
    double g=(kx.back()-kx.front())/kx.size();
    std::vector<piecewise_poly> b,tb;std::vector<double> k;
    k.push_back(kx.front()-g);
    for(int i=0;i<kx.size();i++) k.push_back(kx[i]);
    k.push_back(kx.back()+g);
    for(int i=0;i<k.size()-1;i++)
    {
        std::vector<polynomial> vp={std::vector<double>{0},std::vector<double>{1},std::vector<double>{0}};
        std::vector<double> tk={k[i],k[i+1]};
        b.push_back(piecewise_poly(tk,vp));
    }
    for(int i=0;i<k.size()-2;i++)
    {
        polynomial p1(std::vector<double> {-k[i],1}),p2(std::vector<double> {k[i+2],-1});
        p1=p1*(1/(k[i+1]-k[i]));p2=p2*(1/(k[i+2]-k[i+1]));
        piecewise_poly tpp=b[i]*p1+b[i+1]*p2;
        tb.push_back(tpp);
    }
    b=tb;
    basis=b;
}

void bs_linear::solve_linear()
{
    co=ky;
    piecewise_poly s(std::vector<double> {kx[0]},std::vector<polynomial> {std::vector<double> {0},std::vector<double> {0}});
    for(int i=0;i<basis.size();i++)
    {
        s=s+basis[i]*co[i];
    }
    sp=s;
}

class bs_s32 : public Bspline
{
private:
    std::vector<double> c;
    condition_type ct;
    void get_basis();
    void solve_linear();
public:
    bs_s32(const std::vector<double>& k1,const std::vector<double>& k2,condition_type ct_,std::vector<double>& c_):Bspline(k1,k2),ct(ct_),c(c_)
    {
         if(c_.size()!=2) 
        {
            std::cout<<"The amount of given boundary conditions is not appropriate!";
            std::exit(1);
        }
        this->get_basis();
        this->solve_linear();
    }
    bs_s32(const std::vector<double>& k1,const std::vector<double>& k2,condition_type ct_):Bspline(k1,k2),ct(ct_)
    {
        if(ct!=natural)
        {
            std::cout<<"There is lack of additional boundry conditions!";
            std::exit(1);
        }
        this->get_basis();
        this->solve_linear();
    }
    ~bs_s32(){};
};

void bs_s32::get_basis()
{
    double g=(kx.back()-kx.front())/(kx.size()-1);
    std::vector<piecewise_poly> b;std::vector<double> k;
    k.push_back(kx.front()-double(3)*g);k.push_back(kx.front()-double(2)*g);k.push_back(kx.front()-g);
    for(int i=0;i<kx.size();i++) k.push_back(kx[i]);
    k.push_back(kx.back()+g);k.push_back(kx.back()+double(2)*g);k.push_back(kx.back()+double(3)*g);
    for(int i=0;i<k.size()-1;i++)
    {
        std::vector<polynomial> vp={std::vector<double>{0},std::vector<double>{1},std::vector<double>{0}};
        std::vector<double> tk={k[i],k[i+1]};
        b.push_back(piecewise_poly(tk,vp));
    }
    for(int j=1;j<=3;j++)
    {
        std::vector<piecewise_poly> tb;
        for(int i=0;i<k.size()-j-1;i++)
            {
                polynomial p1(std::vector<double> {-k[i],1}),p2(std::vector<double> {k[i+j+1],-1});
                p1=p1*(1/(k[i+j]-k[i]));p2=p2*(1/(k[i+j+1]-k[i+1]));
                piecewise_poly tpp=b[i]*p1+b[i+1]*p2;
                tb.push_back(tpp);
            }
        b=tb;
    }
    basis=b;
}

void bs_s32::solve_linear()
{
    int dt=ct+1;
    if(ct==natural) 
    {
        c={0,0};
        dt=2;
    }
    int n=basis.size()-2,col=1,info=0;
    double b[n];
    double u[n-1],l[n-1],d[n];
    for(int i=0;i<n-1;i++)
    {
        b[i]=ky[i];d[i]=basis[i+1].value(kx[i]);
        l[i]=basis[i+1].value(kx[i+1]);
        u[i]=basis[i+2].value(kx[i]);
        if(i==0)
        {
            double k=basis[i].value(kx[i]);
            k/=basis[i].derivative(dt,kx[i]);
            d[i]-=k*(basis[i+1].derivative(dt,kx[i]));
            u[i]-=k*(basis[i+2].derivative(dt,kx[i]));
            b[i]-=k*c[0];
        }
        if(i==n-2)
        {
            double k=basis.back().value(kx.back());
            k/=basis.back().derivative(dt,kx.back());
            d[i+1]=basis[n].value(kx.back())-k*(basis[n].derivative(dt,kx.back()));
            l[i]-=k*(basis[n-1].derivative(dt,kx.back()));
            b[i+1]=ky.back()-k*c[1];
        }
    }
    
    dgtsv_(&n,&col,l,d,u,b,&n,&info);
    if(info!=0)
    {
        std::cout<<"Something is wrong with the calculation driven by lapack! Since the INFO is "<<info;
        std::exit(1);
    }
    double k=ky.front()-b[0]*basis[1].value(kx.front())-b[1]*basis[2].value(kx.front());k/=basis[0].value(kx.front());
    co.push_back(k);
    for(int i=0;i<n;i++) co.push_back(b[i]);
    k=ky.back()-b[n-1]*basis[n].value(kx.back())-b[n-2]*basis[n-1].value(kx.back());k/=basis[n+1].value(kx.back());
    co.push_back(k);

    piecewise_poly s(std::vector<double> {kx[0]},std::vector<polynomial> {std::vector<double> {0},std::vector<double> {0}});
    for(int i=0;i<co.size();i++)
    {
        s=s+basis[i]*co[i];
    }
    sp=s;
}