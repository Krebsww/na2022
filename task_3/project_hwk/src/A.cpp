#include<iostream>
#include"ppform.h"
#include<vector>
#include<cmath>
#include<fstream>
using namespace std;
double f(double x)
{
    return 1/(1+25*x*x);
}
double df(double x)
{
    return -50*x/pow(1+25*x*x,2);
}
int main()
{
    ofstream fout;
    fout.open("A.txt",ios::out);
    int N[]={5,10,20,40,80,160,320,640,1280};
    vector<double> bc={df(-1),df(1)};
    condition_type ct=complete;  
    vector<double> e;
    for( int i=1;i<=9;i++)
    {
        vector<double> x;vector<double> y;
        double g=double(2)/N[i-1];
        for(int j=0;j<=N[i-1];j++)
        {
            double t=-1+j*g;
            x.push_back(t);y.push_back(f(t));
        }
        pp_s32 pps(x,y,ct,bc);
        double em=0;
        for(int j=1;j<=N[i-1];j++)
        {
            double te,t=(x[j]+x[j-1])/2;
            te=abs(f(t)-pps.value(t));
            if(te>em) em=te;
        }
        e.push_back(em);
    }
    fout<<"插值点翻倍前后误差之比："<<endl;
    for(int i=1;i<e.size();i++)
    {
        fout<<e[i]/e[i-1]<<endl;
    }
    fout.close();
    return 0;
}