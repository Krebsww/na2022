#include<iostream>
#include<fstream>
#include<cmath>
using namespace std;
double f(double x)
{
    double y=0,a[9]={1,-8,28,-56,70,-56,28,-8,1};
    for(int i=0;i<=8;i++)
    {
        y+=a[i]*pow(x,i);
    }
    return y;
}

double g(double x)
{
    double y=0,a[9]={1,-8,28,-56,70,-56,28,-8,1};
    for(int i=8;i>0;i--)
    {
        y+=a[i];
        y*=x;
    }
    y+=a[0];
    return y;
}

double h(double x)
{
    double y=1;
    for(int i=1;i<=8;i++) y*=(x-1);
    return y;
}

int main()
{
    ofstream fout;
    fout.open("Aout.txt",ios::out);
    double a=0.99,b=1.01,t;
    for(int i=0;i<=100;i++)
    {
        t=(b-a)/100*i+a;
        fout<<f(t)<<","<<g(t)<<","<<h(t)<<endl;
    }
    fout.close();
    return 0;
}