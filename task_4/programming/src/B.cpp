#include<iostream>
#include<fstream>
#include<cmath>
#include<vector>
#define pre 3
using namespace std;
double dec(int * m,int b,int p)
{
    double y=0;
    for(int i=0;i<p;i++)
    {
        y+=m[i]*pow(b,-i);
    }
    return y;
}
int main()
{
    ofstream fout;
    fout.open("F.txt",ios::out);
    static int m[pre];m[0]=1;
    int b=2,L=-1,U=1;
    double UFL,OFL;
    
    /// @brief compute UFL and OFL
    /// @return cout

    UFL=pow(b,L);OFL=pow(b,U)*(b-pow(b,1-pre));
    cout<<"the UFL is "<<UFL<<endl<<"the OFL is "<<OFL<<endl;

    /// @brief enumerate F
    /// @return fstream and cout

    vector<double> v,M;
    double ofl=b-pow(b,1-pre),t=dec(m,b,pre);
    while(t<ofl)
    {
        v.push_back(t);
        m[pre-1]++;
        int f=pre-1;
        while(m[f]==b&&f>0)
        {
            m[f]=0;
            m[f-1]++;
            f--;
        }
        t=dec(m,b,pre);
    }
    v.push_back(ofl);
    int s=v.size();
    for(int e=L;e<=U;e++)
    {
        for(int i=0;i<s;i++) M.push_back((v[i])*pow(b,e));
    }
    for(vector<double>::reverse_iterator i=M.rbegin();i!=M.rend();i++) fout<<-(*i)<<",";
    fout<<double(0)<<",";
    for(vector<double>::iterator i=M.begin();i!=M.end();i++) fout<<*i<<",";
    cout<<"the cardinality of F is "<<(M.size())*2+1;
    fout.close();fout.clear();

    /// @brief enumerate subnormal members
    /// @return fstream

    fout.open("SF.txt",ios::out);
    for(vector<double>::reverse_iterator i=v.rbegin();i!=v.rend()-1;i++) fout<<-(*i-1)*pow(b,L)<<",";
    for(vector<double>::iterator i=v.begin()+1;i!=v.end();i++) fout<<(*i-1)*pow(b,L)<<",";
    fout.close();
    return 0;
}
