#include"polynomial.h"
#include<vector>
#include<iostream>

class piecewise_poly
{
private:
    std::vector<double> knots;
    std::vector<polynomial> poly;
public:
    piecewise_poly(){};
    piecewise_poly(const std::vector<double>& k,const std::vector<polynomial>& p);
    ~piecewise_poly(){};
    double value(double x);
    piecewise_poly operator +(const piecewise_poly& y);
    piecewise_poly operator =(const piecewise_poly& y);
    piecewise_poly operator *(const piecewise_poly& y);
    piecewise_poly operator *(const polynomial& y);
    piecewise_poly operator *(const double& c);
    friend piecewise_poly operator *(const double& c,piecewise_poly& y);
    piecewise_poly derivative(int d);
    double derivative(int d,double x);
    inline void print();
};

piecewise_poly::piecewise_poly(const std::vector<double>& k ,const std::vector<polynomial>& p):knots(k),poly(p)
{
    int lk=k.size(),lp=p.size();
    if(lk+1!=lp) 
    {
        std::cout<<"The amount of knots does not match that of given polynomials!";
        std::exit(1);
    }
    for(int i=0;i<lk-1;i++)
    {
        if(k[i]>=k[i+1])
        {
            std::cout<<"The knots are not sorted!";
            std::exit(1);
        } 
    }
        
}

piecewise_poly piecewise_poly::operator +(const piecewise_poly& y)
{
    std::vector<double> k;
    std::vector<polynomial> p;
    int kx=(this->knots).size(),ky=y.knots.size(),i=0,j=0;
    while (i<kx||j<ky)
    {
        p.push_back(this->poly[i]+y.poly[j]);
        if(i==kx)
        {
            while(j<ky)
            {
                k.push_back(y.knots[j]);
                j++; 
                p.push_back(this->poly[i]+y.poly[j]);
            }
            break;
        }
        if(j==ky)
        {
            while(i<kx)
            {
                k.push_back(this->knots[i]);
                i++;
                p.push_back(this->poly[i]+y.poly[j]);
            }
            break;
        }
           
        if(this->knots[i]<y.knots[j]) 
        {
            k.push_back(this->knots[i]);
            i++;
        }
        else if(this->knots[i]>y.knots[j])
            {
                k.push_back(y.knots[j]);
                j++;
            }
            else{
                k.push_back(y.knots[j]);
                i++;j++;
            }
    }
    return piecewise_poly(k,p);

}

piecewise_poly piecewise_poly::operator =(const piecewise_poly& y)
{
    this->knots.assign(y.knots.begin(),y.knots.end());
    this->poly.assign(y.poly.begin(),y.poly.end());
    return *this;
}

void piecewise_poly::print()
{
    for(std::vector<double>::iterator i=this->knots.begin();i!=this->knots.end();i++) std::cout<<*i<<" ";
    std::cout<<std::endl;
    for(std::vector<polynomial>::iterator i=this->poly.begin();i!=this->poly.end();i++) (*i).print();
}

piecewise_poly piecewise_poly::operator *(const piecewise_poly& y)
 {
    std::vector<double> k;
    std::vector<polynomial> p;
    int kx=(this->knots).size(),ky=y.knots.size(),i=0,j=0;
    while (i<kx||j<ky)
    {
        p.push_back(this->poly[i]*y.poly[j]);
        if(i==kx)
        {
            while(j<ky)
            {
                k.push_back(y.knots[j]);
                j++; 
                p.push_back(this->poly[i]*y.poly[j]);
            }
            break;
        }
        if(j==ky)
        {
            while(i<kx)
            {
                k.push_back(this->knots[i]);
                i++;
                p.push_back(this->poly[i]*y.poly[j]);
            }
            break;
        }
           
        if(this->knots[i]<y.knots[j]) 
        {
            k.push_back(this->knots[i]);
            i++;
        }
        else if(this->knots[i]>y.knots[j])
            {
                k.push_back(y.knots[j]);
                j++;
            }
            else{
                k.push_back(y.knots[j]);
                i++;j++;
            }
    }
    return piecewise_poly(k,p);
 }

 piecewise_poly piecewise_poly::operator *(const polynomial& y)
 {
    std::vector<polynomial> tp;
    for(int i=0;i<poly.size();i++) tp.push_back(poly[i]*y);
    return piecewise_poly(knots,tp);
 }

piecewise_poly piecewise_poly::operator *(const double& c)
 {
    std::vector<double> k;
    std::vector<polynomial> p;
    for(std::vector<polynomial>::iterator i=this->poly.begin();i!=this->poly.end();i++) p.push_back((*i)*c);
    k.assign(this->knots.begin(),this->knots.end());
    return piecewise_poly(k,p);
 }

 piecewise_poly operator *(const double& c,piecewise_poly& y)
 {
    std::vector<double> k;
    std::vector<polynomial> p;
    for(int i=0;i<y.poly.size();i++) p.push_back((y.poly[i])*c);
    k.assign(y.knots.begin(),y.knots.end());
    return piecewise_poly(k,p);
 }

double piecewise_poly::value(double x)
{
    for(int i=0;i<this->knots.size();i++)
    {
        if(x<=this->knots[i]) return this->poly[i].value(x);
    }
    return this->poly.back().value(x);
 }

 piecewise_poly piecewise_poly::derivative(int d)
 {
    std::vector<polynomial> tp;
    for(int i=0;i<poly.size();i++)
    {
        tp.push_back(poly[i].derivative(d));
    }
    return piecewise_poly(knots,tp);
 }

 double piecewise_poly::derivative(int d,double x)
 {
    return (this->derivative(d)).value(x);
 }