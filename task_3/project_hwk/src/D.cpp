#include<iostream>
#include "Bspline.h"
#include<vector>
#include<cmath>
#include<fstream>
#include<limits>
using namespace std;
double f(double x)
{
    return 1/(1+x*x);
}
double df(double x)
{
    return -2*x/pow(1+x*x,2);
}
int main()
{
    ofstream fout;
    fout.open("D.txt",ios::out);
    vector<double> bc={df(-5),df(5)};
    condition_type ct=complete;
    vector<double> x;vector<double> y;
    for(int j=1;j<=11;j++)
    {
        double t=-6+j;
        x.push_back(t);y.push_back(f(t));
    }
    bs_s32 b32(x,y,ct,bc);
    double s[]={-3.5,-3,-0.5,0,0.5,3,3.5};
    int n=200;
    double sg=double(10)/n;
    fout<<"各点对应绝对误差如下："<<endl;
    for(int j=0;j<=6;j++)
    {
        double t=abs(b32.value(s[j])-f(s[j]));
        fout<<s[j]<<" "<<t<<endl;
    }
   /*  for(int j=0;j<=n;j++)
    {
        double t=-5+j*sg;
        double e=abs(b32.value(t)-f(t));
        fout<<t<<" "<<e<<endl;
    } */
    fout.close();
    return 0;
}