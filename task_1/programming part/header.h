#include<iostream>
#include<cmath>
#include<limits>
#define eps std::numeric_limits<double>::epsilon()

class EquationSolver
{
public:
    EquationSolver(double d ,int m):delta(d),M(m) {}
    ~EquationSolver() {}
    virtual double solve(double a, double b, double (*f)(double),double (*g) (double)) =0;
protected:
    double delta;
    int M;
};

class bisection:public EquationSolver
{    
public:
    bisection(double d ,int m):EquationSolver(d,m){}
    double solve(double a, double b, double (*f)(double),double (*g) (double)=nullptr);
};

double bisection::solve(double a, double b, double (*f)(double),double (*g) (double))
{
        double h,u,w,c;
        h=b-a;u=f(a);
        for(int k=1;k<=M;k++)
        {
            h/=2;
            c=a+h;
            w=f(c);
            if(fabs(h)<delta || fabs(w)<eps)
                {
                    break;
                }
            else if (w*u>0)
            {
                a=c;
            }
            
        }
        return c;
}

class newton:public EquationSolver
{
private:
    
public:
    newton(double d ,int m):EquationSolver(d,m){}
    ~newton(){}
    double solve(double a,double b,double (*f)(double),double (*g)(double)=nullptr);
};

double newton::solve(double a,double b,double (*f)(double),double(*g)(double))
{
    if(a!=b) 
    {
        std::cout<<"Wrong parameters!";
        exit(1);
    }
    double u,x=a;
    int k;
    for(k=1;k<=M;k++)
    {
        u=f(x);
        if(fabs(u)<eps) break;
        x-=u/g(x);
    }
    return x;
}

class secant:EquationSolver
{
private:
    /* data */
public:
    secant(double d ,int m):EquationSolver(d,m){};
    ~secant(){}
   double solve(double a,double b,double (*f)(double),double (*g)(double)=nullptr);
};

double secant::solve(double a,double b,double (*f)(double),double (*g)(double))
{
    double x=a,y=b,u=f(b),v=f(a);
    for(int k=2;k<=M;k++)
    {
        double s;
        if(fabs(u)>fabs(v))
        {
            double t;
            t=u;u=v;v=t;
            t=x;x=y;y=t;
        }
        s=(y-x)/(u-v);
        v=u;x=y;
        y=y-u*s;
        u=f(y);
        if(fabs(y-x)<delta || fabs(u)<eps) break;
    }
    return y;
}



