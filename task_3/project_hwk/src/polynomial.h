#ifndef POLYNOMIAL
#define POLYNOMIAL
#include<vector>
#include<iostream>
#include<cmath>
class polynomial
{
private:
    std::vector<double> a;
    int n;
public:
    polynomial(){};
    polynomial(const std::vector<double>& A):a(A),n(A.size()-1){};
    ~polynomial(){};
    double value(const double& x);
    polynomial operator +(const polynomial& y);
    polynomial operator -(const polynomial& y);
    polynomial operator *(const polynomial& y);
    friend polynomial operator*(double c,polynomial& y);
    polynomial operator*(double c);
    polynomial& operator =(const polynomial& y);
    polynomial derivative(int d);
    double derivative(int d,double x);
    inline void print();
};


double polynomial::value(const double& x)
{
    double y=0;
    for(int i=0;i<=n;i++) y+=(this->a)[i]*pow(x,i);
    return y;
}

polynomial polynomial::operator +(const polynomial& y)
{
    int m,M;
    m=(this->n>y.n) ? y.n:this->n;M=(this->n>y.n) ? this->n:y.n;
    std::vector<double> z;
    for(int i=0;i<=m;i++) z.push_back((this->a)[i]+(y.a)[i]);
    if(M==this->n) for(int i=m+1;i<=M;i++) z.push_back((this->a)[i]);
    else for(int i=m+1;i<=M;i++) z.push_back((y.a)[i]);

    while (z.back()==double(0)&&z.size()>=2)
    {
        z.pop_back();
    }
    
    return polynomial(z);
}

polynomial polynomial::operator -(const polynomial& y)
{
    int m,M;
    m=(this->n>y.n) ? y.n:this->n;M=(this->n>y.n) ? this->n:y.n;
    std::vector<double> z;
    for(int i=0;i<=m;i++) z.push_back((this->a)[i]-(y.a)[i]);
    if(M==this->n) for(int i=m+1;i<=M;i++) z.push_back((this->a)[i]);
    else for(int i=m+1;i<=M;i++) z.push_back(-(y.a)[i]);

    while (z.back()==double(0)&&z.size()>=2)
    {
        z.pop_back();
    }
    
    return polynomial(z);
}

polynomial polynomial::operator *(const polynomial& y)
{
    int m=(this->n)+y.n;
    std::vector<double> z;
    for(int i=0;i<=m;i++)
    {
        double s=0;
        for(int j=0;j<=i;j++) 
        {
            if(j<=this->n && i-j<=y.n) s+=((this->a)[j])*((y.a)[i-j]);
            else if(j>this->n) break; 
            else continue;
            
        }
        z.push_back(s);
    }

    while (z.back()==double(0)&&z.size()>=2)
    {
        z.pop_back();
    }
    
    return polynomial(z);
}

polynomial operator*(double c,polynomial& y)
{
    if(c==double(0))  return polynomial(std::vector<double> {0});
    std::vector<double> z;
    for(std::vector<double>::iterator i=(y.a).begin();i!=(y.a).end();i++) z.push_back((*i)*c);
    return polynomial(z);
}

polynomial polynomial::operator*(double c)
{
    if(c==double(0))  return polynomial(std::vector<double> {0});
    std::vector<double> z;
    for(std::vector<double>::iterator i=(this->a).begin();i!=(this->a).end();i++) z.push_back((*i)*c);
    return polynomial(z);
}

polynomial& polynomial::operator =(const polynomial& y)
{
    this->a.assign(y.a.begin(),y.a.end());
    this->n=(this->a).size()-1;
    return *this;
}
void polynomial::print()
{
    for(std::vector<double>::iterator i=(this->a).begin();i!=(this->a).end();i++) std::cout<<*i<<" ";
    std::cout<<std::endl;
}

polynomial polynomial::derivative(int d)
{
    if(d>n) return polynomial(std::vector<double> {0});
    std::vector<double> ta=a;
    for(int i=1;i<=d;i++)
    {
        std::vector<double> t;
        for(int j=1;j<ta.size();j++)
        {
            t.push_back(double(j)*ta[j]);
        } 
        ta=t;
    }
    return polynomial(ta);
}

double polynomial::derivative(int d,double x)
{
    return (this->derivative(d)).value(x);
}
#endif