#include<iostream>
#include "Bspline.h"
#include<vector>
#include<cmath>
#include<fstream>
using namespace std;
double f(double x)
{
    return 1/(1+x*x);
}
double df(double x)
{
    return -2*x/pow(1+x*x,2);
}
int main()
{
    ofstream fout;
    fout.open("C.txt",ios::out);
    vector<double> bc={df(-5),df(5)};
    condition_type ct=complete;
    vector<double> x;vector<double> y;
    for(int j=1;j<=11;j++)
    {
        double t=-6+j;
        x.push_back(t);y.push_back(f(t));
    }
    bs_s32 b32(x,y,ct,bc);
    int n=100;
    double g=double(10)/n;
    fout<<"在等距选取的100个点处，插值函数的值为："<<endl;
    for(int j=0;j<=n;j++)
    {
        double t=double(-5)+g*j;
        fout<<t<<" "<<b32.value(t)<<endl;
    }
    fout.close();
    return 0;
}