#include<iostream>
#include<fstream>
#include<cmath>
#include"Newton.h"
using namespace std;
double f(double x);
int main()
{
    ofstream fout;
    fout.open("Cout.txt",ios::out);
    for(int j=5;j<=20;j+=5)
    {
        const int n=j;
        Newton N;
        double x[n];
        for(int i=0;i<n;i++) x[i]=cos((2.0*(i+1)-1)/2/n*M_PI);
        N.inter(f,x,n);
        for(int i=0;i<=100;i++)
        {
            double t_x=-1.0+2.0*i/100.0;
            fout<<N.pre(t_x)<<",";
        }
        fout<<endl;
    }
    fout.close();
    return 0;
}

double f(double x)
{
    return 1/(1+25*x*x);
}
